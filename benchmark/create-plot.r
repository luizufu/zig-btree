#!/usr/bin/env Rscript

library(ggplot2)
library(reshape2)

data <- read.csv("results.csv")
data$maxNodeSize <- factor(data$maxNodeSize)
data <- melt(
  data,
  id = c("maxNodeSize", "numberOfInserts"), value.name = "time"
)
data$time <- data$time / 1000000
data$variable <- factor(data$variable, labels = c("Insert", "Find", "Remove"))



g <- ggplot(data, aes(numberOfInserts, time, colour = maxNodeSize)) +
  geom_line() +
  facet_grid(rows = vars(variable), scales = "free") +
  xlab("Number of Elements") +
  ylab("Time (ms)")

ggsave("plot.jpg", g)
