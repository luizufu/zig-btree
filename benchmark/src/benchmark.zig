const std = @import("std");
const math = std.math;
const io = std.io;
const time = std.time;
const Allocator = std.mem.Allocator;
const ArrayList = std.ArrayList;
const Order = math.Order;
const BtreeWithMaxNodeSize = @import("btree").BtreeWithMaxNodeSize;

const stdout = io.getStdOut().writer();

pub fn main() anyerror!void {
    const max_node_sizes = [_]comptime_int{ 128, 256, 512, 1024, 2048, 4096, 8192, 16384 };
    const number_of_inserts = [_]comptime_int{ 1000, 10000, 100000, 1000000, 10000000 };
    const allocator = std.heap.c_allocator;

    try stdout.print("{s},{s},{s},{s},{s}\n", .{
        "maxNodeSize",
        "numberOfInserts",
        "insertTime",
        "findTime",
        "removeTime",
    });

    inline for (max_node_sizes) |max_node_size|
        inline for (number_of_inserts) |numberOfInsert|
            try run(numberOfInsert, max_node_size, allocator);
}

fn run(number_of_inserts: usize, comptime max_node_size: comptime_int, allocator: Allocator) !void {
    const keys = try createRandArrayList(allocator, number_of_inserts);
    const values = try createRandArrayList(allocator, number_of_inserts);
    defer keys.deinit();
    defer values.deinit();

    var tree = try BtreeWithMaxNodeSize(u32, u32, max_node_size, void, lessThan).init(allocator, {});
    defer tree.deinit(allocator);

    var timer = try time.Timer.start();
    for (keys.items, 0..) |key, i|
        std.mem.doNotOptimizeAway(try tree.insert(key, &values.items[i], allocator));
    const insert_time = timer.read();

    timer.reset();
    for (keys.items) |key| {
        std.mem.doNotOptimizeAway(tree.find(key));
    }
    const find_time = timer.read();

    timer.reset();
    for (keys.items) |key| {
        std.mem.doNotOptimizeAway(tree.remove(key, allocator));
    }
    const remove_time = timer.read();

    try stdout.print("{},{},{},{},{}\n", .{
        max_node_size,
        number_of_inserts,
        insert_time,
        find_time,
        remove_time,
    });
}

fn lessThan(context: void, a: u32, b: u32) Order {
    _ = context;
    return std.math.order(a, b);
}

fn createRandArrayList(allocator: Allocator, n: usize) !ArrayList(u32) {
    var list = ArrayList(u32).init(allocator);

    var i: usize = 0;
    while (i < n) : (i += 1)
        try list.append(@intCast(i));

    var prng = std.rand.DefaultPrng.init(0);
    const random = prng.random();
    random.shuffle(u32, list.items);

    return list;
}
