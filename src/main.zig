const std = @import("std");
const testing = std.testing;
const assert = std.debug.assert;
const Allocator = std.mem.Allocator;
const ArrayList = std.ArrayList;
const btree = @import("btree/btree.zig");

pub const BtreeWithMaxNodeSize = btree.BtreeWithMaxNodeSize;
pub const Btree = btree.Btree;

fn lessThan(context: void, a: u32, b: u32) std.math.Order {
    _ = context;
    return std.math.order(a, b);
}

test "Btree basic" {
    const allocator = testing.allocator;
    var bt = try Btree(u32, u32, 8, void, lessThan).init(allocator, {});
    defer bt.deinit(allocator);

    const keys = try createRandArrayList(allocator, 10000);
    const values = try createRandArrayList(allocator, 10000);
    defer keys.deinit();
    defer values.deinit();

    const min_key = std.mem.min(u32, keys.items[0..]);
    const max_key = std.mem.max(u32, keys.items[0..]);

    for (keys.items, 0..) |key, i|
        try testing.expect(try bt.insert(key, &values.items[i], allocator));

    try testing.expectEqual(bt.len, @as(u32, @intCast(keys.items.len)));
    try testing.expectEqual(bt.min().?.key, min_key);
    try testing.expectEqual(bt.max().?.key, max_key);

    for (keys.items, 0..) |key, i| {
        const val = bt.find(key);
        try testing.expect(val != null);
        try testing.expectEqual(val.?, &values.items[i]);
        try testing.expectEqual(val.?.*, values.items[i]);
    }

    {
        var counter: u32 = 0;
        var last_key_found: u32 = 0;
        var it = bt.iterator();
        while (it.next()) |entry| {
            try testing.expect(entry.key >= last_key_found);
            last_key_found = entry.key;
            counter += 1;
        }

        try testing.expectEqual(counter, bt.len);
    }

    for (keys.items, 0..) |key, i| {
        var iterator = bt.lowerBoundIterator(key);
        if (iterator.next()) |entry| {
            try testing.expectEqual(entry.key, key);
            try testing.expectEqual(entry.value, &values.items[i]);
        }
    }

    try testing.expect(bt.tryUpdate(@as(u32, @intCast(keys.items.len)), &values.items[0]) == null);

    for (keys.items, 0..) |key, i| {
        const old_value = try bt.insertOrUpdate(key, &values.items[i], allocator);
        try testing.expect(old_value != null);
        try testing.expectEqual(old_value.?, &values.items[i]);
    }

    const mid: u32 = @intCast(keys.items.len / 2);

    for (keys.items[mid..], 0..) |key, i| {
        const val = try bt.remove(key, allocator);
        try testing.expect(val != null);
        try testing.expectEqual(val.?, &values.items[mid + i]);
        try testing.expectEqual(val.?.*, values.items[mid + i]);
    }

    try testing.expectEqual(bt.len, mid);

    for (keys.items[0..mid], 0..) |key, i| {
        const val = bt.find(key);
        try testing.expect(val != null);
        try testing.expectEqual(val.?, &values.items[i]);
        try testing.expectEqual(val.?.*, values.items[i]);
    }

    for (keys.items[mid..]) |key| {
        const val = bt.find(key);
        try testing.expect(val == null);
    }

    for (keys.items[0..mid], 0..) |key, i| {
        const val = try bt.remove(key, allocator);
        try testing.expect(val != null);
        try testing.expectEqual(val.?, &values.items[i]);
        try testing.expectEqual(val.?.*, values.items[i]);
    }

    try testing.expectEqual(bt.len, 0);
}

test "Btree concatenate" {
    try testConcatenate(0, 0);
    try testConcatenate(1, 1);
    try testConcatenate(0, 5);
    try testConcatenate(5, 5);
    try testConcatenate(5, 0);
    try testConcatenate(10000, 0);
    try testConcatenate(9995, 5);
    try testConcatenate(9950, 50);
    try testConcatenate(9800, 200);
    try testConcatenate(9500, 500);
    try testConcatenate(9000, 1000);
    try testConcatenate(8000, 2000);
    try testConcatenate(7000, 3000);
    try testConcatenate(6000, 4000);
    try testConcatenate(5000, 5000);
    try testConcatenate(4000, 6000);
    try testConcatenate(3000, 7000);
    try testConcatenate(2000, 8000);
    try testConcatenate(1000, 9000);
    try testConcatenate(500, 9500);
    try testConcatenate(200, 9800);
    try testConcatenate(50, 9950);
    try testConcatenate(5, 9995);
    try testConcatenate(0, 10000);
}

test "Btree split" {
    try testSplit(6, 0, 0);
    try testSplit(6, 0, 3);
    try testSplit(6, 10, 0);
    try testSplit(6, 3, 0);
    try testSplit(10, 5, 0);
    try testSplit(20, 10, 0);
    try testSplit(40, 15, 0);
    try testSplit(40, 16, 0);
    try testSplit(40, 30, 0);
    try testSplit(10000, 0, 50);
    try testSplit(10000, 50, 50);
    try testSplit(10000, 100, 50);
    try testSplit(10000, 1000, 50);
    try testSplit(10000, 2000, 50);
    try testSplit(10000, 5000, 50);
    try testSplit(10000, 8000, 50);
    try testSplit(10000, 9000, 50);
    try testSplit(10000, 9900, 50);
    try testSplit(10000, 10050, 50);
    try testSplit(10000, 10100, 50);
}

test "Btree removeRange" {
    try testRemoveRange(10, 5, 7, 0);
    try testRemoveRange(8, 5, 7, 0);
    try testRemoveRange(8, 1, 3, 0);
    try testRemoveRange(8, 0, 3, 0);
    try testRemoveRange(8, 3, 8, 0);
    try testRemoveRange(10000, 0, 3000, 0);
    try testRemoveRange(10000, 1000, 4000, 0);
    try testRemoveRange(10000, 2000, 5000, 0);
    try testRemoveRange(10000, 3000, 6000, 0);
    try testRemoveRange(10000, 4000, 7000, 0);
    try testRemoveRange(10000, 5000, 8000, 0);
    try testRemoveRange(10000, 6000, 9000, 0);
    try testRemoveRange(10000, 7000, 10000, 0);
}

test "BtreeWithMaxNodeSize" {
    const allocator = testing.allocator;
    var bt = try BtreeWithMaxNodeSize(u32, u32, 96, void, lessThan).init(allocator, {});
    defer bt.deinit(allocator);
    try testing.expectEqual(@TypeOf(bt), Btree(u32, u32, 8, void, lessThan));
}

fn testConcatenate(leftSize: u32, rightSize: u32) !void {
    const allocator = testing.allocator;
    var left_tree = try Btree(u32, u32, 8, void, lessThan).init(allocator, {});
    defer left_tree.deinit(allocator);
    var right_tree = try Btree(u32, u32, 8, void, lessThan).init(allocator, {});

    const left_keys = try createSortedArrayList(allocator, leftSize, 0);
    const left_values = try createSortedArrayList(allocator, leftSize, 0);
    defer left_keys.deinit();
    defer left_values.deinit();

    const right_keys = try createSortedArrayList(allocator, rightSize, @intCast(leftSize));
    const right_values = try createSortedArrayList(allocator, rightSize, @intCast(leftSize));
    defer right_keys.deinit();
    defer right_values.deinit();

    for (left_keys.items, 0..) |key, i|
        _ = try left_tree.insert(key, &left_values.items[i], allocator);

    for (right_keys.items, 0..) |key, i|
        _ = try right_tree.insert(key, &right_values.items[i], allocator);

    try left_tree.concatenate(&right_tree, allocator);

    try testing.expectEqual(left_tree.len, @as(u32, @intCast(left_keys.items.len + right_keys.items.len)));

    for (left_keys.items, 0..) |key, i| {
        const val = left_tree.find(key);
        try testing.expect(val != null);
        try testing.expectEqual(val.?, &left_values.items[i]);
        try testing.expectEqual(val.?.*, left_values.items[i]);
    }

    for (right_keys.items, 0..) |key, i| {
        const val = left_tree.find(key);
        try testing.expect(val != null);
        try testing.expectEqual(val.?, &right_values.items[i]);
        try testing.expectEqual(val.?.*, right_values.items[i]);
    }

    {
        var keys = if (leftSize > 0) &left_keys else &right_keys;
        var start: u32 = 0;
        var counter: u32 = 0;
        var last_key_found: u32 = 0;
        var it = left_tree.iterator();
        while (it.next()) |entry| {
            try testing.expect(entry.key >= last_key_found);
            try testing.expectEqual(entry.key, keys.*.items[counter - start]);
            last_key_found = entry.key;
            counter += 1;
            if (counter == left_keys.items.len) {
                keys = &right_keys;
                start = @intCast(left_keys.items.len);
            }
        }

        try testing.expectEqual(counter, left_tree.len);
    }
}

fn testSplit(size: u32, splitKey: u32, startKey: u32) !void {
    const allocator = testing.allocator;
    var bt = try Btree(u32, u32, 8, void, lessThan).init(allocator, {});
    defer bt.deinit(allocator);

    const keys = try createSortedArrayList(allocator, size, startKey);
    const values = try createSortedArrayList(allocator, size, startKey);
    defer keys.deinit();
    defer values.deinit();

    for (keys.items, 0..) |key, i|
        _ = try bt.insert(key, &values.items[i], allocator);

    var other = try bt.split(splitKey, allocator);
    defer other.deinit(allocator);

    if (bt.min()) |entry| {
        try testing.expectEqual(entry.key, keys.items[0]);
    }

    if (bt.max()) |entry| {
        try testing.expect(entry.key < splitKey);
    }

    if (other.min()) |entry| {
        try testing.expect(entry.key >= splitKey);
    }

    if (other.max()) |entry| {
        try testing.expectEqual(entry.key, keys.items[keys.items.len - 1]);
    }

    bt.recomputeLen();
    other.recomputeLen();

    var first_half_size: u32 = 0;
    while (first_half_size < keys.items.len and keys.items[first_half_size] < splitKey)
        first_half_size += 1;

    try testing.expectEqual(bt.len, first_half_size);
    try testing.expectEqual(other.len, @as(u32, @intCast(keys.items.len - first_half_size)));

    {
        var i: u32 = 0;
        while (i < first_half_size) : (i += 1) {
            var val = bt.find(keys.items[i]);
            try testing.expect(val != null);
            try testing.expectEqual(val.?, &values.items[i]);
            try testing.expectEqual(val.?.*, values.items[i]);

            val = other.find(keys.items[i]);
            try testing.expect(val == null);
        }

        while (i < keys.items.len) : (i += 1) {
            var val = bt.find(keys.items[i]);
            try testing.expect(val == null);

            val = other.find(keys.items[i]);
            try testing.expect(val != null);
            try testing.expectEqual(val.?, &values.items[i]);
            try testing.expectEqual(val.?.*, values.items[i]);
        }
    }

    {
        var counter: u32 = 0;
        var last_key_found: u32 = 0;
        var it = bt.iterator();
        while (it.next()) |entry| {
            try testing.expect(entry.key >= last_key_found);
            try testing.expectEqual(entry.key, keys.items[counter]);
            last_key_found = entry.key;
            counter += 1;
        }

        it = other.iterator();
        while (it.next()) |entry| {
            try testing.expect(entry.key >= last_key_found);
            try testing.expectEqual(entry.key, keys.items[counter]);
            last_key_found = entry.key;
            counter += 1;
        }

        try testing.expectEqual(counter, @as(u32, @intCast(keys.items.len)));
    }
}

fn testRemoveRange(size: u32, leftKey: u32, rightKey: u32, startKey: u32) !void {
    const allocator = testing.allocator;
    var bt = try Btree(u32, u32, 8, void, lessThan).init(allocator, {});
    defer bt.deinit(allocator);

    const keys = try createSortedArrayList(allocator, size, startKey);
    defer keys.deinit();
    const values = try createSortedArrayList(allocator, size, startKey);
    defer values.deinit();

    for (keys.items, 0..) |key, i|
        _ = try bt.insert(key, &values.items[i], allocator);

    var removed_tree = try bt.removeRange(leftKey, rightKey, allocator);
    try removed_tree.recycleTreeNodes(allocator);
    try testing.expectEqual(bt.len, size - (rightKey - leftKey));

    {
        var i: u32 = startKey;
        while (i < leftKey) : (i += 1) {
            const val = bt.find(i);
            try testing.expect(val != null);
            try testing.expectEqual(val.?, &values.items[i - startKey]);
        }

        while (i < rightKey) : (i += 1) {
            const val = bt.find(i);
            try testing.expect(val == null);
        }

        while (i < keys.items.len + startKey) : (i += 1) {
            const val = bt.find(i);
            try testing.expect(val != null);
            try testing.expectEqual(val.?, &values.items[i - startKey]);
        }
    }

    {
        var counter: u32 = 0;
        var it = bt.iterator();
        while (it.next()) |entry| {
            try testing.expect(entry.key < leftKey or entry.key >= rightKey);
            counter += 1;
        }

        try testing.expectEqual(counter, size - (rightKey - leftKey));
    }
}

fn createRandArrayList(allocator: Allocator, n: u32) !ArrayList(u32) {
    var list = ArrayList(u32).init(allocator);

    var i: u32 = 0;
    while (i < n) : (i += 1)
        try list.append(@intCast(i));

    var prng = std.rand.DefaultPrng.init(0);
    const random = prng.random();
    random.shuffle(u32, list.items);

    return list;
}

fn createSortedArrayList(allocator: Allocator, n: u32, start: u32) !ArrayList(u32) {
    var list = ArrayList(u32).init(allocator);

    var i: u32 = 0;
    while (i < n) : (i += 1)
        try list.append(@intCast(start + i));

    return list;
}
