const std = @import("std");

const Allocator = std.mem.Allocator;
const ArrayList = std.ArrayList;

pub fn StaticStorage(comptime TraversalEntry: type, comptime Node: type, comptime destroyFn: fn (*Node, Allocator) void) type {
    return struct {
        pub var stack: ArrayList(TraversalEntry) = undefined;
        pub var stack2: ArrayList(TraversalEntry) = undefined;
        pub var reusable: ArrayList(usize) = undefined;
        var n_instances: usize = 0;

        pub fn register(allocator: Allocator) void {
            if (n_instances == 0) {
                stack = ArrayList(TraversalEntry).init(allocator);
                stack2 = ArrayList(TraversalEntry).init(allocator);
                reusable = ArrayList(usize).init(allocator);
            }
            n_instances += 1;
        }

        pub fn unregister(allocator: Allocator) void {
            n_instances -= 1;
            if (n_instances == 0) {
                for (reusable.items) |address|
                    destroyFn(@ptrFromInt(address), allocator);

                stack.deinit();
                stack2.deinit();
                reusable.deinit();
            }
        }
    };
}
