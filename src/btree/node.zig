const std = @import("std");
const testing = std.testing;
const assert = std.debug.assert;
const mem = std.mem;
const ArrayList = std.ArrayList;
const Allocator = mem.Allocator;
const Order = std.math.Order;

pub fn Node(
    comptime K: type,
    comptime b: comptime_int,
    comptime Context: type,
    comptime compareFn: fn (Context, K, K) Order,
) type {
    assert(b > 4);
    const half_b = b / 2;
    return struct {
        const Self = @This();
        const Type = enum(u1) { internal, external };
        const Info = packed struct {
            current_size: u31,
            type: Type,
        };

        children: [b]usize,
        keys: [b - 1]K,
        info: Info,

        pub fn initLeaf() Self {
            var node: Self = .{
                .keys = undefined,
                .children = undefined,
                .info = .{ .current_size = 0, .type = .external },
            };
            node.children[b - 1] = 0;

            return node;
        }

        pub fn initNonLeaf() Self {
            return .{
                .keys = undefined,
                .children = undefined,
                .info = .{ .current_size = 0, .type = .internal },
            };
        }

        pub fn initNewRoot(key: K, left_address: usize, right_address: usize) Self {
            var node: Self = .{
                .keys = undefined,
                .children = undefined,
                .info = .{ .current_size = 2, .type = .internal },
            };
            node.keys[0] = key;
            node.children[0] = left_address;
            node.children[1] = right_address;

            return node;
        }

        pub fn lowerBound(self: Self, key: K, context: Context) u31 {
            var i: u31 = 0;
            const is_non_leaf: u31 = @intFromBool(!self.isLeaf());
            while (i < self.info.current_size - is_non_leaf and compareFn(context, self.keys[i], key) == .lt)
                i += 1;

            return i;
        }

        pub fn upperBound(self: Self, key: K, context: Context) u31 {
            var i: u31 = 0;
            const is_non_leaf: u31 = @intFromBool(!self.isLeaf());
            while (i < self.info.current_size - is_non_leaf and compareFn(context, self.keys[i], key) != .gt)
                i += 1;

            return i;
        }

        pub fn insertIntoLeaf(self: *Self, i: u31, key: K, child_address: usize) void {
            assert(self.isLeaf());
            assert(i <= self.info.current_size);

            mem.copyBackwards(K, self.keys[i + 1 ..], self.keys[i..self.info.current_size]);
            self.keys[i] = key;
            mem.copyBackwards(usize, self.children[i + 1 ..], self.children[i..self.info.current_size]);
            self.children[i] = child_address;
            self.info.current_size += 1;
        }

        pub fn insertIntoNonLeaf(self: *Self, i: u31, key: K, child_address: usize) void {
            assert(!self.isLeaf());
            assert(i <= self.info.current_size);

            mem.copyBackwards(K, self.keys[i..], self.keys[i - 1 .. self.info.current_size - 1]);
            self.keys[i - 1] = key;
            mem.copyBackwards(usize, self.children[i + 1 ..], self.children[i..self.info.current_size]);
            self.children[i] = child_address;
            self.info.current_size += 1;
        }

        pub fn removeFromLeaf(self: *Self, i: u31) void {
            assert(self.isLeaf());
            assert(i < self.info.current_size);

            mem.copyForwards(K, self.keys[i..], self.keys[i + 1 .. self.info.current_size]);
            mem.copyForwards(usize, self.children[i..], self.children[i + 1 .. self.info.current_size]);
            self.info.current_size -= 1;
        }

        pub fn removeFromLeafUntil(self: *Self, i: u31, j: u31) void {
            assert(self.isLeaf());
            assert(i < self.info.current_size);

            const n = j - i;
            mem.copyForwards(K, self.keys[i..], self.keys[i + n .. self.info.current_size]);
            mem.copyForwards(usize, self.children[i..], self.children[i + n .. self.info.current_size]);
            self.info.current_size -= n;
        }

        pub fn removeFromNonLeaf(self: *Self, i: u31) void {
            assert(!self.isLeaf());
            assert(i < self.info.current_size);

            mem.copyForwards(K, self.keys[i - 1 ..], self.keys[i .. self.info.current_size - 1]);
            mem.copyForwards(usize, self.children[i..], self.children[i + 1 .. self.info.current_size]);
            self.info.current_size -= 1;
        }

        pub fn removeFromNonLeafUntil(self: *Self, i: u31, j: u31) void {
            assert(!self.isLeaf());
            assert(i < self.info.current_size);

            const n = j - i;
            mem.copyForwards(K, self.keys[i - 1 ..], self.keys[i + n - 1 .. self.info.current_size - 1]);
            mem.copyForwards(usize, self.children[i..], self.children[i + n .. self.info.current_size]);
            self.info.current_size -= n;
        }

        pub fn partitionLeafAt(self: *Self, other: *Self, i: u31) void {
            assert(self.isLeaf());
            assert(other.isLeaf());
            assert(other.info.current_size == 0);

            mem.copyForwards(K, other.keys[0..], self.keys[i..self.info.current_size]);
            mem.copyForwards(usize, other.children[0..], self.children[i..self.info.current_size]);
            other.children[b - 1] = self.children[b - 1];
            self.children[b - 1] = 0;

            other.info.current_size = self.info.current_size - i;
            self.info.current_size = i;
        }

        pub fn partitionNonLeafAt(self: *Self, other: *Self, i: u31) void {
            assert(!self.isLeaf());
            assert(!other.isLeaf());
            assert(other.info.current_size == 0);

            if (i < self.info.current_size - 1) {
                mem.copyForwards(K, other.keys[0..], self.keys[i + 1 .. self.info.current_size - 1]);
            }
            mem.copyForwards(usize, other.children[0..], self.children[i + 1 .. self.info.current_size]);
            other.info.current_size = self.info.current_size - (i + 1);
            self.info.current_size = i;
        }

        pub fn splitInsertingIntoLeaf(self: *Self, other: *Self, i: u31, key: K, address: usize) K {
            assert(self.isLeaf());
            assert(other.isLeaf());
            assert(self.isFull());
            assert(other.info.current_size == 0);

            // if inserting left, pass one more element to other
            const insert_left: u31 = @intFromBool(i <= half_b);

            mem.copyForwards(K, other.keys[0..], self.keys[half_b - insert_left .. self.info.current_size]);
            mem.copyForwards(usize, other.children[0..], self.children[half_b - insert_left .. self.info.current_size]);
            other.children[b - 1] = self.children[b - 1];
            self.children[b - 1] = @intFromPtr(other);

            other.info.current_size = self.info.current_size - half_b + insert_left;
            self.info.current_size = half_b - insert_left;

            if (i <= self.info.current_size) {
                self.insertIntoLeaf(i, key, address);
            } else {
                other.insertIntoLeaf(i - self.info.current_size, key, address);
            }

            return other.keys[0];
        }

        pub fn splitInsertingIntoNonLeaf(self: *Self, other: *Self, i: u31, key: K, address: usize) K {
            assert(!self.isLeaf());
            assert(!other.isLeaf());
            assert(self.isFull());
            assert(other.info.current_size == 0);

            // if inserting right, pass one less element to other
            const insert_right: u31 = @intFromBool(i > half_b);

            mem.copyForwards(K, other.keys[0..], self.keys[half_b + insert_right .. self.info.current_size - 1]);
            mem.copyForwards(usize, other.children[0..], self.children[half_b + insert_right .. self.info.current_size]);

            other.info.current_size = self.info.current_size - half_b - insert_right;
            self.info.current_size = half_b + insert_right;
            const parent_key = self.keys[self.info.current_size - 1];

            if (i <= self.info.current_size) {
                self.insertIntoNonLeaf(i, key, address);
            } else {
                other.insertIntoNonLeaf(i - self.info.current_size, key, address);
            }

            return parent_key;
        }

        pub fn borrowNFromLeaf(self: *Self, other: *Self, n: u31) K {
            assert(self.isLeaf());
            assert(other.isLeaf());
            assert((n < other.info.current_size and self.isEmpty()) or n == other.info.current_size);
            assert((n < other.info.current_size and other.canShare()) or (n == other.info.current_size and self.canMergeWith(other.*)));

            mem.copyForwards(K, self.keys[self.info.current_size..], other.keys[0..n]);
            mem.copyForwards(K, other.keys[0..], other.keys[n..other.info.current_size]);
            mem.copyForwards(usize, self.children[self.info.current_size..], other.children[0..n]);
            mem.copyForwards(usize, other.children[0..], other.children[n..other.info.current_size]);

            self.info.current_size += n;
            other.info.current_size -= n;

            return other.keys[0];
        }

        pub fn borrowNFromNonLeaf(self: *Self, other: *Self, n: u31, old_parent_key: K) K {
            assert(!self.isLeaf());
            assert(!other.isLeaf());
            assert((n < other.info.current_size and self.isEmpty()) or n == other.info.current_size);
            assert((n < other.info.current_size and other.canShare()) or (n == other.info.current_size and self.canMergeWith(other.*)));

            self.keys[self.info.current_size - 1] = old_parent_key;
            mem.copyForwards(K, self.keys[self.info.current_size..], other.keys[0..n]);
            if (n < other.info.current_size) {
                mem.copyForwards(K, other.keys[0..], other.keys[n .. other.info.current_size - 1]);
            }
            mem.copyForwards(usize, self.children[self.info.current_size..], other.children[0..n]);
            mem.copyForwards(usize, other.children[0..], other.children[n..other.info.current_size]);

            self.info.current_size += n;
            other.info.current_size -= n;

            return self.keys[self.info.current_size - 1];
        }

        pub fn shareNWithLeaf(self: *Self, other: *Self, n: u31) K {
            assert(self.isLeaf());
            assert(other.isLeaf());
            assert(self.canShare());
            assert(other.isEmpty());

            mem.copyBackwards(K, other.keys[n..], other.keys[0..other.info.current_size]);
            mem.copyForwards(K, other.keys[0..], self.keys[self.info.current_size - n .. self.info.current_size]);
            mem.copyBackwards(usize, other.children[n..], other.children[0..other.info.current_size]);
            mem.copyForwards(usize, other.children[0..], self.children[self.info.current_size - n .. self.info.current_size]);
            self.info.current_size -= n;
            other.info.current_size += n;
            return other.keys[0];
        }

        pub fn shareNWithNonLeaf(self: *Self, other: *Self, n: u31, old_parent_key: K) K {
            assert(!self.isLeaf());
            assert(!other.isLeaf());
            assert(self.canShare());
            assert(other.isEmpty());

            mem.copyBackwards(K, other.keys[n..], other.keys[0 .. other.info.current_size - 1]);
            other.keys[n - 1] = old_parent_key;
            mem.copyForwards(K, other.keys[0..], self.keys[self.info.current_size - n .. self.info.current_size - 1]);
            mem.copyBackwards(usize, other.children[n..], other.children[0..other.info.current_size]);
            mem.copyForwards(usize, other.children[0..], self.children[self.info.current_size - n .. self.info.current_size]);

            self.info.current_size -= n;
            other.info.current_size += n;

            return self.keys[self.info.current_size - 1];
        }

        pub fn borrowFromLeaf(self: *Self, other: *Self) K {
            assert(self.isLeaf());
            assert(other.isLeaf());
            assert(self.info.current_size < other.info.current_size);

            const half = (self.info.current_size + other.info.current_size) / 2;
            return self.borrowNFromLeaf(other, other.info.current_size - half);
        }

        pub fn borrowFromNonLeaf(self: *Self, other: *Self, old_parent_key: K) K {
            assert(!self.isLeaf());
            assert(!other.isLeaf());
            assert(self.info.current_size < other.info.current_size);

            const half = (self.info.current_size + other.info.current_size) / 2;
            return self.borrowNFromNonLeaf(other, other.info.current_size - half, old_parent_key);
        }

        pub fn shareWithLeaf(self: *Self, other: *Self) K {
            assert(self.isLeaf());
            assert(other.isLeaf());
            assert(self.info.current_size > other.info.current_size);

            const half = (self.info.current_size + other.info.current_size) / 2;
            return self.shareNWithLeaf(other, self.info.current_size - half);
        }

        pub fn shareWithNonLeaf(self: *Self, other: *Self, old_parent_key: K) K {
            assert(!self.isLeaf());
            assert(!other.isLeaf());
            assert(self.info.current_size > other.info.current_size);

            const half = (self.info.current_size + other.info.current_size) / 2;
            return self.shareNWithNonLeaf(other, self.info.current_size - half, old_parent_key);
        }

        pub fn mergeLeaf(self: *Self, other: *Self) void {
            assert(self.isLeaf());
            assert(other.isLeaf());
            assert(self.canMergeWith(other.*));

            const n = other.info.current_size;
            mem.copyForwards(K, self.keys[self.info.current_size..], other.keys[0..n]);
            mem.copyForwards(usize, self.children[self.info.current_size..], other.children[0..n]);
            self.info.current_size += n;
            other.info.current_size -= n;
            self.children[b - 1] = other.children[b - 1];
        }

        pub fn mergeNonLeaf(self: *Self, other: *Self, old_parent_key: K) void {
            assert(!self.isLeaf());
            assert(!other.isLeaf());
            assert(self.canMergeWith(other.*));

            const n = other.info.current_size;
            self.keys[self.info.current_size - 1] = old_parent_key;
            mem.copyForwards(K, self.keys[self.info.current_size..], other.keys[0 .. n - 1]);
            mem.copyForwards(usize, self.children[self.info.current_size..], other.children[0..n]);
            self.info.current_size += n;
            other.info.current_size -= n;
        }

        pub fn isLeaf(self: Self) bool {
            return self.info.type == .external;
        }

        pub fn isFull(self: Self) bool {
            return self.info.current_size + @intFromBool(self.isLeaf()) == b;
        }

        pub fn isEmpty(self: Self) bool {
            return self.info.current_size + @intFromBool(self.isLeaf()) < half_b;
        }

        pub fn canShare(self: Self) bool {
            return self.info.current_size + @intFromBool(self.isLeaf()) > half_b;
        }

        pub fn canMergeWith(self: Self, other: Self) bool {
            const selfSize = self.info.current_size + @intFromBool(self.isLeaf());
            const otherSize = other.info.current_size + @intFromBool(other.isLeaf());
            return selfSize + otherSize <= b;
        }
    };
}

test "Node.initLeaf" {
    const b = 8;

    const n = Node(u32, b, void, lessThan).initLeaf();

    try testing.expect(n.isLeaf());
    try testing.expect(n.isEmpty());
    try testing.expect(!n.isFull());
    try testing.expect(!n.canShare());
    try testing.expectEqual(n.info.current_size, @as(u31, @intCast(0)));
    try testing.expectEqual(n.children[b - 1], 0);
}

test "Node.initNewRoot" {
    const b = 8;
    const n = Node(u32, b, void, lessThan).initNewRoot(5, 0, 0);

    try testing.expect(!n.isLeaf());
    try testing.expect(!n.isFull());
    try testing.expect(!n.canShare());
    try testing.expectEqual(n.info.current_size, @as(u31, @intCast(2)));
}

test "Node.insertIntoLeaf/removeFromLeaf" {
    const allocator = testing.allocator;
    const b = 1000;

    var keys = try createRandArrayList(u32, allocator, b - 1);
    defer keys.deinit();
    var values = try createRandArrayList(usize, allocator, b - 1);
    defer values.deinit();
    var n = initLeaf1000Test(keys.items[0..], values.items[0..]);

    try testing.expect(n.isFull());
    try testing.expect(!n.isEmpty());
    try testing.expect(n.canShare());
    try testing.expectEqual(n.info.current_size, @as(u31, @intCast(values.items.len)));
    try testing.expectEqual(n.children[b - 1], 0);

    for (values.items, 0..) |value, i| {
        try testing.expectEqual(n.keys[i], keys.items[i]);
        try testing.expectEqual(n.children[i], value);
    }

    {
        var i: u31 = @intCast(values.items.len);
        while (i > 10) : (i -= 10) {
            try testing.expectEqual(n.info.current_size, @as(u31, @intCast(values.items.len)));
            n.removeFromLeaf(i - 1);
            _ = keys.orderedRemove(i - 1);
            _ = values.orderedRemove(i - 1);
        }
    }

    try testing.expectEqual(n.info.current_size, @as(u31, @intCast(values.items.len)));

    for (values.items, 0..) |value, i| {
        try testing.expectEqual(n.keys[i], keys.items[i]);
        try testing.expectEqual(n.children[i], value);
    }
}

test "Node.insertoIntoNonLeaf/removeFromNonLeaf" {
    const allocator = testing.allocator;
    const b = 1000;

    var keys = try createRandArrayList(u32, allocator, b - 1);
    defer keys.deinit();
    var values = try createRandArrayList(usize, allocator, b);
    defer values.deinit();
    var n = initNonLeaf1000Test(keys.items[0..], values.items[0..]);

    try testing.expect(n.isFull());
    try testing.expect(!n.isEmpty());
    try testing.expect(n.canShare());
    try testing.expectEqual(n.info.current_size, @as(u31, @intCast(values.items.len)));

    for (keys.items, 0..) |key, i| {
        try testing.expectEqual(n.keys[i], key);
    }
    for (values.items, 0..) |value, i| {
        try testing.expectEqual(n.children[i], value);
    }

    {
        var i: u31 = @intCast(values.items.len);
        while (i > 10) : (i -= 10) {
            try testing.expectEqual(n.info.current_size, @as(u31, @intCast(values.items.len)));
            n.removeFromNonLeaf(i - 1);
            _ = keys.orderedRemove(i - 2);
            _ = values.orderedRemove(i - 1);
        }
    }

    try testing.expectEqual(n.info.current_size, @as(u31, @intCast(values.items.len)));

    for (keys.items, 0..) |key, i| {
        try testing.expectEqual(n.keys[i], key);
    }
    for (values.items, 0..) |value, i| {
        try testing.expectEqual(n.children[i], value);
    }
}

test "Node.splitInsertingIntoLeaf" {
    const allocator = testing.allocator;
    const b = 1000;
    const mid = b / 2;
    const new_key: u32 = 777;
    const new_value: usize = 999;

    var keys = try createRandArrayList(u32, allocator, b);
    defer keys.deinit();
    var values = try createRandArrayList(usize, allocator, b);
    defer values.deinit();

    var n1 = initLeaf1001Test(keys.items[0..], values.items[0..]);
    var n2 = Node(u32, b + 1, void, lessThan).initLeaf();

    const parent_key = n1.splitInsertingIntoLeaf(&n2, b, new_key, new_value);
    try testing.expectEqual(parent_key, keys.items[mid]);
    try testing.expectEqual(n1.info.current_size + n2.info.current_size, @as(u31, @intCast(b + 1)));

    var i: u31 = 0;
    while (i < n1.info.current_size) : (i += 1) {
        try testing.expectEqual(n1.keys[i], keys.items[i]);
        try testing.expectEqual(n1.children[i], values.items[i]);
    }

    i = 0;
    while (i < n2.info.current_size - 1) : (i += 1) {
        try testing.expectEqual(n2.keys[i], keys.items[mid + i]);
        try testing.expectEqual(n2.children[i], values.items[mid + i]);
    }

    try testing.expectEqual(n2.keys[i], new_key);
    try testing.expectEqual(n2.children[i], new_value);
}

test "Node.splitInsertingIntoNonLeaf" {
    const allocator = testing.allocator;
    const b = 1000;
    const mid = b / 2;
    const new_key: u32 = 777;
    const new_value: usize = 999;

    var keys = try createRandArrayList(u32, allocator, b - 1);
    defer keys.deinit();
    var values = try createRandArrayList(usize, allocator, b);
    defer values.deinit();

    var n1 = initNonLeaf1000Test(keys.items[0..], values.items[0..]);
    var n2 = Node(u32, b, void, lessThan).initNonLeaf();

    const parent_key = n1.splitInsertingIntoNonLeaf(&n2, b, new_key, new_value);
    try testing.expectEqual(parent_key, keys.items[mid]);
    try testing.expectEqual(n1.info.current_size + n2.info.current_size, @as(u31, @intCast(b + 1)));

    var i: u31 = 0;
    while (i < n1.info.current_size - 1) : (i += 1) {
        try testing.expectEqual(n1.keys[i], keys.items[i]);
        try testing.expectEqual(n1.children[i], values.items[i]);
    }
    try testing.expectEqual(n1.children[i], values.items[i]);

    i = 0;
    while (i < n2.info.current_size - 2) : (i += 1) {
        try testing.expectEqual(n2.keys[i], keys.items[mid + i + 1]);
        try testing.expectEqual(n2.children[i], values.items[mid + i + 1]);
    }
    try testing.expectEqual(n2.children[i], values.items[mid + i + 1]);

    try testing.expectEqual(n2.keys[i], new_key);
    try testing.expectEqual(n2.children[i + 1], new_value);
}

test "Node.borrowFromLeaf" {
    const allocator = testing.allocator;
    const b = 1000;
    const mid = b / 2;

    var keys = try createRandArrayList(u32, allocator, b - 2);
    defer keys.deinit();
    var values = try createRandArrayList(usize, allocator, b - 2);
    defer values.deinit();

    var n1 = initLeaf1000Test(keys.items[0 .. mid - 2], values.items[0 .. mid - 2]);
    var n2 = initLeaf1000Test(keys.items[mid - 2 ..], values.items[mid - 2 ..]);

    try testing.expectEqual(n1.info.current_size + 1, @as(u31, @intCast(499)));
    try testing.expectEqual(n2.info.current_size + 1, @as(u31, @intCast(501)));
    try testing.expect(n1.isEmpty());
    try testing.expect(n2.canShare());

    const parent_key = n1.borrowFromLeaf(&n2);
    try testing.expectEqual(parent_key, keys.items[mid - 1]);
    try testing.expectEqual(n1.info.current_size + 1, @as(u31, @intCast(500)));
    try testing.expectEqual(n2.info.current_size + 1, @as(u31, @intCast(500)));

    var i: u31 = 0;
    while (i < n1.info.current_size) : (i += 1) {
        try testing.expectEqual(n1.keys[i], keys.items[i]);
        try testing.expectEqual(n1.children[i], values.items[i]);
    }

    i = 0;
    while (i < n2.info.current_size) : (i += 1) {
        try testing.expectEqual(n2.keys[i], keys.items[mid + i - 1]);
        try testing.expectEqual(n2.children[i], values.items[mid + i - 1]);
    }
}

test "Node.borrowFromNonLeaf" {
    const allocator = testing.allocator;
    const b = 1000;
    const mid = b / 2;

    var keys = try createRandArrayList(u32, allocator, b - 2);
    defer keys.deinit();
    var values = try createRandArrayList(usize, allocator, b);
    defer values.deinit();

    var n1 = initNonLeaf1000Test(keys.items[0 .. mid - 2], values.items[0 .. mid - 1]);
    var n2 = initNonLeaf1000Test(keys.items[mid - 2 ..], values.items[mid - 1 ..]);

    try testing.expectEqual(n1.info.current_size, @as(u31, @intCast(499)));
    try testing.expectEqual(n2.info.current_size, @as(u31, @intCast(501)));
    try testing.expect(n1.isEmpty());
    try testing.expect(n2.canShare());

    const old_parent: u32 = 666777999;

    const parent_key = n1.borrowFromNonLeaf(&n2, old_parent);
    try testing.expectEqual(parent_key, keys.items[mid - 2]);
    try testing.expectEqual(n1.info.current_size, @as(u31, @intCast(500)));
    try testing.expectEqual(n2.info.current_size, @as(u31, @intCast(500)));

    var i: u31 = 0;
    while (i < n1.info.current_size - 2) : (i += 1) {
        try testing.expectEqual(n1.keys[i], keys.items[i]);
        try testing.expectEqual(n1.children[i], values.items[i]);
    }
    try testing.expectEqual(n1.keys[i], old_parent);
    try testing.expectEqual(n1.children[i], values.items[i]);
    try testing.expectEqual(n1.children[i + 1], values.items[i + 1]);

    i = 0;
    while (i < n2.info.current_size - 1) : (i += 1) {
        try testing.expectEqual(n2.keys[i], keys.items[mid + i - 1]);
        try testing.expectEqual(n2.children[i], values.items[mid + i]);
    }
    try testing.expectEqual(n2.children[i], values.items[mid + i]);
}

test "Node.shareWithLeaf" {
    const allocator = testing.allocator;
    const b = 1000;
    const mid = b / 2;

    var keys = try createRandArrayList(u32, allocator, b - 2);
    defer keys.deinit();
    var values = try createRandArrayList(usize, allocator, b - 2);
    defer values.deinit();

    var n1 = initLeaf1000Test(keys.items[0..mid], values.items[0..mid]);
    var n2 = initLeaf1000Test(keys.items[mid..], values.items[mid..]);

    try testing.expectEqual(n1.info.current_size + 1, @as(u31, @intCast(501)));
    try testing.expectEqual(n2.info.current_size + 1, @as(u31, @intCast(499)));
    try testing.expect(n1.canShare());
    try testing.expect(n2.isEmpty());

    const parent_key = n1.shareWithLeaf(&n2);
    try testing.expectEqual(parent_key, keys.items[mid - 1]);
    try testing.expectEqual(n1.info.current_size + 1, @as(u31, @intCast(500)));
    try testing.expectEqual(n2.info.current_size + 1, @as(u31, @intCast(500)));

    var i: u31 = 0;
    while (i < n1.info.current_size) : (i += 1) {
        try testing.expectEqual(n1.keys[i], keys.items[i]);
        try testing.expectEqual(n1.children[i], values.items[i]);
    }

    i = 0;
    while (i < n2.info.current_size) : (i += 1) {
        try testing.expectEqual(n2.keys[i], keys.items[mid + i - 1]);
        try testing.expectEqual(n2.children[i], values.items[mid + i - 1]);
    }
}

test "Node.shareWithNonLeaf" {
    const allocator = testing.allocator;
    const b = 1000;
    const mid = b / 2;

    var keys = try createRandArrayList(u32, allocator, b - 2);
    defer keys.deinit();
    var values = try createRandArrayList(usize, allocator, b);
    defer values.deinit();

    var n1 = initNonLeaf1000Test(keys.items[0..mid], values.items[0 .. mid + 1]);
    var n2 = initNonLeaf1000Test(keys.items[mid..], values.items[mid + 1 ..]);

    try testing.expectEqual(n1.info.current_size, @as(u31, @intCast(501)));
    try testing.expectEqual(n2.info.current_size, @as(u31, @intCast(499)));
    try testing.expect(n1.canShare());
    try testing.expect(n2.isEmpty());

    const old_parent: u32 = 666777999;

    const parent_key = n1.shareWithNonLeaf(&n2, old_parent);
    try testing.expectEqual(parent_key, keys.items[mid - 1]);
    try testing.expectEqual(n1.info.current_size, @as(u31, @intCast(500)));
    try testing.expectEqual(n2.info.current_size, @as(u31, @intCast(500)));

    var i: u31 = 0;
    while (i < n1.info.current_size - 1) : (i += 1) {
        try testing.expectEqual(n1.keys[i], keys.items[i]);
        try testing.expectEqual(n1.children[i], values.items[i]);
    }
    try testing.expectEqual(n1.children[i], values.items[i]);

    try testing.expectEqual(n2.keys[0], old_parent);
    try testing.expectEqual(n2.children[0], keys.items[mid]);
    i = 1;
    while (i < n2.info.current_size - 1) : (i += 1) {
        try testing.expectEqual(n2.keys[i], keys.items[mid + i - 1]);
        try testing.expectEqual(n2.children[i], values.items[mid + i]);
    }
    try testing.expectEqual(n2.children[i], values.items[mid + i]);
}

test "Node.mergeLeaf" {
    const allocator = testing.allocator;
    const b = 1000;
    const mid = b / 2;

    var keys = try createRandArrayList(u32, allocator, b - 3);
    defer keys.deinit();
    var values = try createRandArrayList(usize, allocator, b - 3);
    defer values.deinit();

    var n1 = initLeaf1000Test(keys.items[0 .. mid - 2], values.items[0 .. mid - 2]);
    var n2 = initLeaf1000Test(keys.items[mid - 2 ..], values.items[mid - 2 ..]);

    try testing.expectEqual(n1.info.current_size + 1, @as(u31, @intCast(499)));
    try testing.expectEqual(n2.info.current_size + 1, @as(u31, @intCast(500)));
    try testing.expect(n1.isEmpty());
    try testing.expect(!n2.canShare());

    n1.mergeLeaf(&n2);
    try testing.expectEqual(n1.info.current_size + 1, @as(u31, @intCast(998)));
    try testing.expectEqual(n2.info.current_size, @as(u31, @intCast(0)));

    var i: u31 = 0;
    while (i < n1.info.current_size) : (i += 1) {
        try testing.expectEqual(n1.keys[i], keys.items[i]);
        try testing.expectEqual(n1.children[i], values.items[i]);
    }
}

test "Node.mergeNonLeaf" {
    const allocator = testing.allocator;
    const b = 1000;
    const mid = b / 2;

    var keys = try createRandArrayList(u32, allocator, b - 3);
    defer keys.deinit();
    var values = try createRandArrayList(usize, allocator, b - 1);
    defer values.deinit();

    var n1 = initNonLeaf1000Test(keys.items[0 .. mid - 2], values.items[0 .. mid - 1]);
    var n2 = initNonLeaf1000Test(keys.items[mid - 2 ..], values.items[mid - 1 ..]);

    try testing.expectEqual(n1.info.current_size, @as(u31, @intCast(499)));
    try testing.expectEqual(n2.info.current_size, @as(u31, @intCast(500)));
    try testing.expect(n1.isEmpty());
    try testing.expect(!n2.canShare());

    const old_parent: u32 = 9977531;
    n1.mergeNonLeaf(&n2, old_parent);
    try testing.expectEqual(n1.info.current_size, @as(u31, @intCast(999)));
    try testing.expectEqual(n2.info.current_size, @as(u31, @intCast(0)));

    var i: u31 = 0;
    while (i < @as(u31, @intCast(498))) : (i += 1) {
        try testing.expectEqual(n1.keys[i], keys.items[i]);
        try testing.expectEqual(n1.children[i], values.items[i]);
    }
    try testing.expectEqual(n1.keys[i], old_parent);
    try testing.expectEqual(n1.children[i], values.items[i]);
    i += 1;
    while (i < n1.info.current_size - 1) : (i += 1) {
        try testing.expectEqual(n1.keys[i], keys.items[i - 1]);
        try testing.expectEqual(n1.children[i], values.items[i]);
    }
    try testing.expectEqual(n1.children[i], values.items[i]);
}

test "Note.mergeNonLeaf (full resulting node)" {
    const parent_key = 5;
    const keys = [_]u32{ 1, 2, 3, 4, 6, 7 };
    const values = [_]usize{ 1, 2, 3, 4, 5, 6, 7, 8 };

    var n1 = initNonLeaf8Test(keys[0..4], values[0..5]);
    var n2 = initNonLeaf8Test(keys[4..], values[5..]);

    try testing.expectEqual(n1.info.current_size, 5);
    try testing.expectEqual(n2.info.current_size, 3);

    n1.mergeNonLeaf(&n2, parent_key);
    try testing.expectEqual(n1.keys[0], 1);
    try testing.expectEqual(n1.keys[1], 2);
    try testing.expectEqual(n1.keys[2], 3);
    try testing.expectEqual(n1.keys[3], 4);
    try testing.expectEqual(n1.keys[4], 5);
    try testing.expectEqual(n1.keys[5], 6);
    try testing.expectEqual(n1.keys[6], 7);
}

fn lessThan(context: void, a: u32, b: u32) Order {
    _ = context;
    return std.math.order(a, b);
}

const Node8 = Node(u32, 8, void, lessThan);
const Node999 = Node(u32, 999, void, lessThan);
const Node1000 = Node(u32, 1000, void, lessThan);
const Node1001 = Node(u32, 1001, void, lessThan);

fn initLeaf8Test(keys: []const u32, values: []const usize) Node8 {
    var n = Node8.initLeaf();
    for (values, 0..) |value, i| {
        n.insertIntoLeaf(n.info.current_size, keys[i], value);
    }

    return n;
}

fn initNonLeaf8Test(keys: []const u32, values: []const usize) Node8 {
    var n = Node8.initNewRoot(keys[0], values[0], values[1]);
    var i: u31 = 2;
    while (i < values.len) : (i += 1) {
        n.insertIntoNonLeaf(n.info.current_size, keys[i - 1], values[i]);
    }

    return n;
}

fn initLeaf1000Test(keys: []const u32, values: []const usize) Node1000 {
    var n = Node1000.initLeaf();
    for (values, 0..) |value, i| {
        n.insertIntoLeaf(n.info.current_size, keys[i], value);
    }

    return n;
}

fn initNonLeaf1000Test(keys: []const u32, values: []const usize) Node1000 {
    var n = Node1000.initNewRoot(keys[0], values[0], values[1]);
    var i: u31 = 2;
    while (i < values.len) : (i += 1) {
        n.insertIntoNonLeaf(n.info.current_size, keys[i - 1], values[i]);
    }

    return n;
}

fn initLeaf1001Test(keys: []const u32, values: []const usize) Node1001 {
    var n = Node1001.initLeaf();
    for (values, 0..) |value, i| {
        n.insertIntoLeaf(n.info.current_size, keys[i], value);
    }

    return n;
}

fn initNonLeaf1001Test(keys: []const u32, values: []const usize) Node1001 {
    var n = Node1001.initNewRoot(keys[0], values[0], values[1]);
    var i: u31 = 2;
    while (i < values.len) : (i += 1) {
        n.insertIntoNonLeaf(n.info.current_size, keys[i - 1], values[i]);
    }

    return n;
}

fn createRandArrayList(comptime T: type, allocator: Allocator, n: usize) !ArrayList(T) {
    var list = ArrayList(T).init(allocator);

    var i: usize = 0;
    while (i < n) : (i += 1)
        try list.append(@intCast(i));

    var prng = std.rand.DefaultPrng.init(0);
    const random = prng.random();
    random.shuffle(T, list.items);

    return list;
}
