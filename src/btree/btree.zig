const std = @import("std");
const assert = std.debug.assert;
const Allocator = std.mem.Allocator;
const ArrayList = std.ArrayList;
const Order = std.math.Order;
const NodeImpl = @import("node.zig").Node;
const StaticStorage = @import("static-storage.zig").StaticStorage;

pub fn BtreeWithMaxNodeSize(
    comptime K: type,
    comptime V: type,
    comptime max_size: comptime_int,
    comptime Context: type,
    comptime compareFn: fn (context: Context, a: K, b: K) Order,
) type {
    assert(max_size >= @sizeOf(NodeImpl(K, 5, Context, compareFn)));
    // estimate using optimal padding, b >= realB
    var b = (max_size + @sizeOf(K) - @sizeOf(u32)) / (@sizeOf(usize) + @sizeOf(K));
    while (@sizeOf(NodeImpl(K, b, Context, compareFn)) > max_size)
        b -= 1;

    return Btree(K, V, b, Context, compareFn);
}

pub fn Btree(
    comptime K: type,
    comptime V: type,
    comptime b: comptime_int,
    comptime Context: type,
    comptime compareFn: fn (context: Context, a: K, b: K) Order,
) type {
    assert(b > 4);
    return struct {
        const Self = @This();
        const Node = NodeImpl(K, b, Context, compareFn);
        pub const MapEntry = struct { key: K, value: *V };
        const TraversalEntry = struct { node: *Node, index: u31 };
        const RemovalAction = enum { do_nothing, share_left, share_right, merge_left, merge_right };
        const RemovalStrategy = struct { action: RemovalAction, other: *Node };
        const S = StaticStorage(TraversalEntry, Node, destroyRecursively);

        root: *Node,
        head: *Node,
        tail: *Node,
        height: u8,
        len: u32,
        context: Context,

        pub fn init(allocator: Allocator, context: Context) !Self {
            S.register(allocator);
            const root = try allocator.create(Node);
            errdefer allocator.destroy(root);
            root.* = Node.initLeaf();

            return .{
                .root = root,
                .head = root,
                .tail = root,
                .height = 0,
                .len = 0,
                .context = context,
            };
        }

        pub fn deinit(self: Self, allocator: Allocator) void {
            destroyRecursively(self.root, allocator);
            S.unregister(allocator);
        }

        pub fn find(self: Self, key: K) ?*V {
            if (self.len == 0)
                return null;

            const leaf = stacklessFindLeaf(self.root, key, self.context);
            const index = leaf.lowerBound(key, self.context);
            if (index >= leaf.info.current_size or compareFn(self.context, leaf.keys[index], key) != .eq)
                return null;

            return @ptrFromInt(leaf.children[index]);
        }

        pub fn insert(self: *Self, key: K, value: *V, allocator: Allocator) !bool {
            const leaf = try findLeaf(self.root, key, self.context);
            const index = leaf.lowerBound(key, self.context);
            if (index < leaf.info.current_size and compareFn(self.context, leaf.keys[index], key) == .eq)
                return false;

            self.len += 1;

            var key_to_insert = key;
            var address_to_insert = @intFromPtr(value);

            {
                const entry: TraversalEntry = .{ .node = leaf, .index = index };
                if (try self.insertLeafAndFinalize(entry, &key_to_insert, &address_to_insert, allocator))
                    return true;
            }

            while (S.stack.popOrNull()) |entry| {
                if (try self.insertNonLeafAndFinalize(entry, &key_to_insert, &address_to_insert, allocator))
                    return true;
            }

            const new_root = try createNode(allocator);
            new_root.* = Node.initNewRoot(key_to_insert, @intFromPtr(self.root), address_to_insert);
            self.root = new_root;
            self.height += 1;

            return true;
        }

        pub fn tryUpdate(self: *Self, key: K, value: *V) ?*V {
            if (self.len == 0)
                return null;

            const leaf = stacklessFindLeaf(self.root, key, self.context);
            const index = leaf.lowerBound(key, self.context);
            if (index >= leaf.info.current_size or compareFn(self.context, leaf.keys[index], key) != .eq)
                return null;

            const old_value = leaf.children[index];
            leaf.children[index] = @intFromPtr(value);

            return @ptrFromInt(old_value);
        }

        pub fn insertOrUpdate(self: *Self, key: K, value: *V, allocator: Allocator) !?*V {
            const leaf = try findLeaf(self.root, key, self.context);
            const index = leaf.lowerBound(key, self.context);
            if (index < leaf.info.current_size and compareFn(self.context, leaf.keys[index], key) == .eq) {
                const old_value = leaf.children[index];
                leaf.children[index] = @intFromPtr(value);

                return @ptrFromInt(old_value);
            }

            self.len += 1;

            var key_to_insert = key;
            var address_to_insert = @intFromPtr(value);

            {
                const entry = TraversalEntry{ .node = leaf, .index = index };
                if (try self.insertLeafAndFinalize(entry, &key_to_insert, &address_to_insert, allocator))
                    return null;
            }

            while (S.stack.popOrNull()) |entry| {
                if (try self.insertNonLeafAndFinalize(entry, &key_to_insert, &address_to_insert, allocator))
                    return null;
            }

            const new_root = try createNode(allocator);
            new_root.* = Node.initNewRoot(key_to_insert, @intFromPtr(self.root), address_to_insert);
            self.root = new_root;
            self.height += 1;

            return null;
        }

        pub fn remove(self: *Self, key: K, allocator: Allocator) !?*V {
            if (self.len == 0)
                return null;

            const leaf = try findLeaf(self.root, key, self.context);
            var index_to_remove = leaf.lowerBound(key, self.context);
            if (index_to_remove == leaf.info.current_size or compareFn(self.context, leaf.keys[index_to_remove], key) != .eq)
                return null;

            const value: *V = @ptrFromInt(leaf.children[index_to_remove]);
            self.len -= 1;

            var parent_entry = S.stack.popOrNull();
            if (self.removeLeafAndFinalize(leaf, parent_entry, &index_to_remove, allocator))
                return value;

            while (S.stack.items.len > 0) {
                const node = parent_entry.?.node;
                parent_entry = S.stack.pop();
                if (self.removeNonLeafAndFinalize(node, parent_entry, &index_to_remove, allocator))
                    return value;
            }

            if (self.removeNonLeafAndFinalize(self.root, null, &index_to_remove, allocator))
                if (self.root.info.current_size > 1)
                    return value;

            const new_root: *Node = @ptrFromInt(self.root.children[0]);
            allocator.destroy(self.root);
            self.root = new_root;
            self.height -= 1;

            return value;
        }

        pub fn removeRange(self: *Self, left_key: K, right_key: K, allocator: Allocator) !Self {
            assert(compareFn(self.context, left_key, right_key) == .lt);

            const old_len = self.len;

            var removed_tree = try self.split(left_key, allocator);
            var right_tree = try removed_tree.split(right_key, allocator);
            try self.concatenate(&right_tree, allocator);

            removed_tree.recomputeLen();
            self.len = old_len - removed_tree.len;

            return removed_tree;
        }

        pub fn split(self: *Self, key: K, allocator: Allocator) !Self {
            if (self.min()) |min_entry| {
                if (compareFn(self.context, key, min_entry.key) != .gt) {
                    const right_tree = self.*;
                    self.* = try init(allocator, self.context);
                    return right_tree;
                }
            }

            if (self.max()) |max_entry| {
                if (compareFn(self.context, key, max_entry.key) == .gt)
                    return try init(allocator, self.context);
            }

            var other = try init(allocator, self.context);
            errdefer other.deinit(allocator);

            const old_height = self.height;

            self.root = try findLeaf(self.root, key, self.context);
            self.height = 0;
            other.tail = if (self.root != self.tail) self.tail else other.root;
            self.tail = self.root;
            self.len = std.math.maxInt(u32);
            other.len = std.math.maxInt(u32);

            var index = self.root.lowerBound(key, self.context);
            self.root.partitionLeafAt(other.root, index);

            var left_parent_key = self.root.keys[0];
            var right_parent_key = other.root.keys[0];

            while (S.stack.items.len > 0) {
                const entry = S.stack.pop();

                index = entry.index;
                var left_root = entry.node;
                const right_root = try createNode(allocator);
                right_root.* = Node.initNonLeaf();

                const left_height = old_height - @as(u8, @intCast(S.stack.items.len));
                const right_height = old_height - @as(u8, @intCast(S.stack.items.len));

                if (index > 0)
                    left_parent_key = left_root.keys[index - 1];
                if (index < left_root.info.current_size - 1)
                    right_parent_key = left_root.keys[index];

                left_root.partitionNonLeafAt(right_root, index);

                if (left_root.info.current_size > 0) {
                    var left_tree = try initNode(left_root, left_height, allocator, self.context);
                    const rightmost = try left_tree.joinRight(self, left_parent_key, allocator);
                    if (rightmost.isLeaf()) {
                        self.tail = rightmost;
                        self.tail.children[b - 1] = 0;
                    }
                    self.root = left_tree.root;
                    self.height = left_tree.height;
                } else {
                    allocator.destroy(left_root);
                }

                if (right_root.info.current_size > 0) {
                    var right_tree = try initNode(right_root, right_height, allocator, other.context);
                    if (other.height == right_tree.height) {
                        const rightmost = try other.joinRight(&right_tree, right_parent_key, allocator);
                        if (other.tail == right_tree.root) {
                            other.tail = rightmost;
                            other.tail.children[b - 1] = 0;
                        }
                        right_tree.root = other.root;
                        right_tree.height = other.height;
                    } else {
                        const leftmost = try right_tree.joinLeft(&other, right_parent_key, allocator);
                        if (leftmost.isLeaf())
                            other.head = leftmost;
                    }
                    other.root = right_tree.root;
                    other.height = right_tree.height;
                } else {
                    allocator.destroy(right_root);
                }
            }

            return other;
        }

        pub fn concatenate(self: *Self, other: *Self, allocator: Allocator) !void {
            if (self.len == 0)
                std.mem.swap(Self, self, other);

            if (self.len == std.math.maxInt(u32))
                self.len = 1;

            if (other.len == 0) {
                other.deinit(allocator);
                return;
            }

            if (other.len == std.math.maxInt(u32))
                other.len = 1;

            self.tail.children[b - 1] = @intFromPtr(other.head);

            const parent_key = other.head.keys[0];
            if (self.height < other.height) {
                const leftmost = try other.joinLeft(self, parent_key, allocator);
                other.head = if (leftmost.isLeaf()) leftmost else self.head;
                std.mem.swap(Self, self, other);
            } else {
                const rightmost = try self.joinRight(other, parent_key, allocator);
                self.tail = if (rightmost.isLeaf()) rightmost else other.tail;
                self.tail.children[b - 1] = 0;
            }
            self.len += other.len;
            S.unregister(allocator);
        }

        pub fn recycleTreeNodes(self: *Self, allocator: Allocator) !void {
            if (!self.root.isLeaf())
                try S.reusable.appendSlice(self.root.children[0..self.root.info.current_size]);

            allocator.destroy(self.root);
            S.unregister(allocator);
        }

        pub fn recomputeLen(self: *Self) void {
            var address = @intFromPtr(self.head);
            self.len = 0;
            while (address != 0) {
                const leaf: *Node = @ptrFromInt(address);
                self.len += leaf.info.current_size;
                address = leaf.children[b - 1];
            }
        }

        pub fn min(self: Self) ?MapEntry {
            if (self.len == 0)
                return null;

            return .{
                .key = self.head.keys[0],
                .value = @ptrFromInt(self.head.children[0]),
            };
        }

        pub fn max(self: Self) ?MapEntry {
            if (self.len == 0)
                return null;

            return .{
                .key = self.tail.keys[self.tail.info.current_size - 1],
                .value = @ptrFromInt(self.tail.children[self.tail.info.current_size - 1]),
            };
        }

        pub fn byteSize(self: Self) usize {
            const ByteSizeRec = struct {
                fn compute(node: *Node) usize {
                    var size: usize = @sizeOf(Node);
                    if (!node.isLeaf()) {
                        var i: usize = 0;
                        while (i < node.info.current_size) : (i += 1)
                            size += compute(@ptrFromInt(node.children[i]));
                    }

                    return size;
                }
            };

            return @sizeOf(Self) + ByteSizeRec.compute(self.root);
        }

        pub fn byteSizeWithoutChildPointers(self: Self) usize {
            const ByteSizeRec = struct {
                fn compute(node: *Node) usize {
                    var size: usize = @sizeOf(Node);
                    if (!node.isLeaf()) {
                        var i: usize = 0;
                        while (i < node.info.current_size) : (i += 1)
                            size += compute(@ptrFromInt(node.children[i]));
                    } else {
                        size -= node.info.current_size * @sizeOf(usize);
                    }

                    return size;
                }
            };

            return @sizeOf(Self) + ByteSizeRec.compute(self.root);
        }

        pub fn nodeUtilization(self: Self) f64 {
            const NodeUtilizationRec = struct {
                fn compute(node: *Node) f64 {
                    var utilization: f64 = node.utilization();
                    var i: usize = 0;

                    if (!node.isLeaf()) {
                        while (i < node.info.current_size) : (i += 1)
                            utilization += compute(@ptrFromInt(node.children[i]));
                    }

                    return utilization / @as(f64, @floatFromInt(i + 1));
                }
            };

            return NodeUtilizationRec.compute(self.root);
        }

        pub fn iterator(self: Self) LeafIterator {
            return LeafIterator.init(self.head, 0);
        }

        pub fn lowerBoundIterator(self: Self, key: K) LeafIterator {
            const leaf = stacklessFindLeaf(self.root, key, self.context);
            const index = leaf.lowerBound(key, self.context);
            return LeafIterator.init(leaf, index);
        }

        pub fn upperBoundIterator(self: Self, key: K) LeafIterator {
            const leaf = stacklessFindLeaf(self.root, key, self.context);
            const index = leaf.upperBound(key, self.context);
            return LeafIterator.init(leaf, index);
        }

        pub const LeafIterator = struct {
            node: *Node,
            prevNode: ?*Node,
            index: u31,
            initialNode: *Node,
            initialIndex: u31,

            pub fn init(head: *Node, index: u31) LeafIterator {
                var it: LeafIterator = .{
                    .node = undefined,
                    .index = undefined,
                    .prevNode = undefined,
                    .initialNode = head,
                    .initialIndex = index,
                };

                it.reset();
                return it;
            }

            pub fn next(it: *LeafIterator) ?MapEntry {
                if (it.index >= it.node.info.current_size)
                    return null;

                const result: MapEntry = .{
                    .key = it.node.keys[it.index],
                    .value = @ptrFromInt(it.node.children[it.index]),
                };

                it.index += 1;
                if (it.index >= it.node.info.current_size and it.node.children[b - 1] != 0) {
                    it.prevNode = it.node;
                    it.node = @ptrFromInt(it.node.children[b - 1]);
                    it.index = 0;
                }

                return result;
            }

            pub fn tryMoveBack(it: *LeafIterator) bool {
                if (it.index > 0) {
                    it.index -= 1;
                    return true;
                }

                if (it.prevNode) |prev| {
                    it.node = prev;
                    it.index = it.node.info.current_size - 1;
                    return true;
                }

                return false;
            }

            pub fn reset(it: *LeafIterator) void {
                it.node = it.initialNode;
                it.prevNode = null;
                it.index = it.initialIndex;

                if (it.index >= it.node.info.current_size and it.node.children[b - 1] != 0) {
                    it.prevNode = it.node;
                    it.node = @ptrFromInt(it.node.children[b - 1]);
                    it.index = 0;
                }
            }

            pub fn invalidate(it: *LeafIterator) void {
                it.index = it.node.info.current_size;
            }
        };

        fn createNode(allocator: Allocator) !*Node {
            if (S.reusable.items.len == 0)
                return try allocator.create(Node);

            const n: *Node = @ptrFromInt(S.reusable.pop());
            if (!n.isLeaf())
                try S.reusable.appendSlice(n.children[0..n.info.current_size]);

            return n;
        }

        fn destroyRecursively(node: *Node, allocator: Allocator) void {
            if (!node.isLeaf())
                for (node.children[0..node.info.current_size]) |address|
                    destroyRecursively(@ptrFromInt(address), allocator);

            allocator.destroy(node);
        }

        fn initNode(root: *Node, height: u8, allocator: Allocator, context: Context) !Self {
            var self: Self = .{
                .root = root,
                .height = height,
                .len = undefined,
                .head = undefined,
                .tail = undefined,
                .context = context,
            };

            if (self.root.info.current_size == 1) {
                const childPtr = self.root.children[0];
                allocator.destroy(self.root);
                self.root = @ptrFromInt(childPtr);
                self.height -= 1;
            }

            return self;
        }

        fn stacklessFindLeaf(root: *Node, key: K, context: Context) *Node {
            var node = root;
            while (!node.isLeaf()) {
                const index = node.upperBound(key, context);
                node = @ptrFromInt(node.children[index]);
            }
            return node;
        }

        fn findLeaf(root: *Node, key: K, context: Context) !*Node {
            S.stack.clearRetainingCapacity();
            var node = root;
            while (!node.isLeaf()) {
                const index = node.upperBound(key, context);
                try S.stack.append(.{ .node = node, .index = index });
                node = @ptrFromInt(node.children[index]);
            }
            return node;
        }

        fn findNthRightmostNode(root: *Node, n: u32) !*Node {
            S.stack2.clearRetainingCapacity();
            var node = root;
            var depth = n;
            while (depth > 0 and !node.isLeaf()) : (depth -= 1) {
                const index = node.info.current_size - 1;
                try S.stack2.append(.{ .node = node, .index = index });
                node = @ptrFromInt(node.children[index]);
            }
            return node;
        }

        fn findNthLeftmostNode(root: *Node, n: u32) !*Node {
            S.stack2.clearRetainingCapacity();
            var node = root;
            var depth = n;
            while (depth > 0 and !node.isLeaf()) : (depth -= 1) {
                const index = 0;
                try S.stack2.append(.{ .node = node, .index = index });
                node = @ptrFromInt(node.children[index]);
            }
            return node;
        }

        fn insertLeafAndFinalize(self: *Self, entry: TraversalEntry, key: *K, address: *usize, allocator: Allocator) !bool {
            if (!entry.node.isFull()) {
                entry.node.insertIntoLeaf(entry.index, key.*, address.*);
                return true;
            }

            const right_node = try createNode(allocator);
            right_node.* = Node.initLeaf();
            key.* = entry.node.splitInsertingIntoLeaf(right_node, entry.index, key.*, address.*);
            address.* = @intFromPtr(right_node);

            if (entry.node == self.tail) {
                self.tail = right_node;
            }

            return false;
        }

        fn insertNonLeafAndFinalize(self: *Self, entry: TraversalEntry, key: *K, address: *usize, allocator: Allocator) !bool {
            _ = self;

            if (!entry.node.isFull()) {
                entry.node.insertIntoNonLeaf(entry.index + 1, key.*, address.*);
                return true;
            }

            const right_node = try createNode(allocator);
            right_node.* = Node.initNonLeaf();
            key.* = entry.node.splitInsertingIntoNonLeaf(right_node, entry.index + 1, key.*, address.*);
            address.* = @intFromPtr(right_node);

            return false;
        }

        fn removeLeafAndFinalize(self: *Self, leaf: *Node, parent_entry: ?TraversalEntry, index_to_remove: *u31, allocator: Allocator) bool {
            leaf.removeFromLeaf(index_to_remove.*);

            var removal_strategy = self.setupRemovalStrategy(leaf, parent_entry);
            switch (removal_strategy.action) {
                .share_left => {
                    const parent_key = removal_strategy.other.shareWithLeaf(leaf);
                    parent_entry.?.node.keys[parent_entry.?.index - 1] = parent_key;
                    return true;
                },
                .share_right => {
                    const parent_key = leaf.borrowFromLeaf(removal_strategy.other);
                    parent_entry.?.node.keys[parent_entry.?.index] = parent_key;
                    return true;
                },
                .merge_left => {
                    removal_strategy.other.mergeLeaf(leaf);
                    allocator.destroy(leaf);
                    index_to_remove.* = parent_entry.?.index;

                    if (leaf == self.tail) {
                        self.tail = removal_strategy.other;
                    }

                    return false;
                },
                .merge_right => {
                    leaf.mergeLeaf(removal_strategy.other);
                    allocator.destroy(removal_strategy.other);
                    index_to_remove.* = parent_entry.?.index + 1;
                    return false;
                },
                else => {
                    return true;
                },
            }
        }

        fn removeNonLeafAndFinalize(self: Self, node: *Node, parent_entry: ?TraversalEntry, index_to_remove: *u31, allocator: Allocator) bool {
            node.removeFromNonLeaf(index_to_remove.*);

            const removal_strategy = self.setupRemovalStrategy(node, parent_entry);
            switch (removal_strategy.action) {
                .share_left => {
                    const parent_key = &parent_entry.?.node.keys[parent_entry.?.index - 1];
                    parent_key.* = removal_strategy.other.shareWithNonLeaf(node, parent_key.*);
                    return true;
                },
                .share_right => {
                    const parent_key = &parent_entry.?.node.keys[parent_entry.?.index];
                    parent_key.* = node.borrowFromNonLeaf(removal_strategy.other, parent_key.*);
                    return true;
                },
                .merge_left => {
                    const parent_key = parent_entry.?.node.keys[parent_entry.?.index - 1];
                    removal_strategy.other.mergeNonLeaf(node, parent_key);
                    allocator.destroy(node);
                    index_to_remove.* = parent_entry.?.index;
                    return false;
                },
                .merge_right => {
                    const parent_key = parent_entry.?.node.keys[parent_entry.?.index];
                    node.mergeNonLeaf(removal_strategy.other, parent_key);
                    allocator.destroy(removal_strategy.other);
                    index_to_remove.* = parent_entry.?.index + 1;
                    return false;
                },
                else => {
                    return true;
                },
            }
        }

        fn setupRemovalStrategy(self: Self, node: *const Node, parent_entry: ?TraversalEntry) RemovalStrategy {
            if (node == self.root or !node.isEmpty() or parent_entry == null)
                return .{ .action = .do_nothing, .other = undefined };

            const parent_node = parent_entry.?.node;
            const parent_index = parent_entry.?.index;
            const left_node = if (parent_index > 0) @as(*Node, @ptrFromInt(parent_node.children[parent_index - 1])) else null;
            const right_node = if (parent_index + 1 < parent_node.info.current_size) @as(*Node, @ptrFromInt(parent_node.children[parent_index + 1])) else null;

            if (left_node) |left|
                if (left.canShare())
                    return .{ .action = .share_left, .other = left };

            if (right_node) |right|
                if (right.canShare())
                    return .{ .action = .share_right, .other = right };

            if (left_node) |left|
                return .{ .action = .merge_left, .other = left };

            if (right_node) |right|
                return .{ .action = .merge_right, .other = right };

            unreachable;
        }

        fn joinRight(self: *Self, other: *Self, parent_key: K, allocator: Allocator) !*Node {
            const left_node = try findNthRightmostNode(self.root, self.height - other.height);
            const right_root = other.root;
            assert(left_node.info.type == right_root.info.type);

            if (right_root.isEmpty() and left_node.canMergeWith(right_root.*)) {
                if (left_node.isLeaf()) {
                    left_node.mergeLeaf(right_root);
                } else {
                    left_node.mergeNonLeaf(right_root, parent_key);
                }
                allocator.destroy(right_root);
                return left_node;
            }

            var key_to_insert = parent_key;
            var address_to_insert = @intFromPtr(right_root);

            if (right_root.isEmpty()) {
                if (left_node.isLeaf()) {
                    key_to_insert = left_node.shareWithLeaf(right_root);
                } else {
                    key_to_insert = left_node.shareWithNonLeaf(right_root, key_to_insert);
                }
            }

            var entry = TraversalEntry{ .node = left_node, .index = undefined };
            while (S.stack2.items.len > 0) {
                entry = S.stack2.pop();
                if (try self.insertNonLeafAndFinalize(entry, &key_to_insert, &address_to_insert, allocator))
                    return right_root;
            }

            const new_root = try createNode(allocator);
            new_root.* = Node.initNewRoot(key_to_insert, @intFromPtr(entry.node), address_to_insert);
            self.root = new_root;
            self.height += 1;
            return right_root;
        }

        fn joinLeft(self: *Self, other: *Self, parent_key: K, allocator: Allocator) !*Node {
            const left_root = other.root;
            const right_node = try findNthLeftmostNode(self.root, self.height - other.height);
            assert(left_root.info.type == right_node.info.type);

            if (S.stack2.items.len > 0) {
                var parent_entry = S.stack2.items[S.stack2.items.len - 1];
                parent_entry.node.children[parent_entry.index] = @intFromPtr(left_root);
            }

            if (left_root.isEmpty() and left_root.canMergeWith(right_node.*)) {
                if (left_root.isLeaf()) {
                    left_root.mergeLeaf(right_node);
                } else {
                    left_root.mergeNonLeaf(right_node, parent_key);
                }
                allocator.destroy(right_node);
                return left_root;
            }

            var key_to_insert = parent_key;
            var address_to_insert = @intFromPtr(right_node);

            if (left_root.isEmpty()) {
                if (left_root.isLeaf()) {
                    key_to_insert = left_root.borrowFromLeaf(right_node);
                } else {
                    key_to_insert = left_root.borrowFromNonLeaf(right_node, key_to_insert);
                }
            }

            var entry: TraversalEntry = .{ .node = right_node, .index = undefined };
            while (S.stack2.items.len > 0) {
                entry = S.stack2.pop();
                if (try self.insertNonLeafAndFinalize(entry, &key_to_insert, &address_to_insert, allocator))
                    return left_root;
            }

            const new_root = try createNode(allocator);
            new_root.* = Node.initNewRoot(key_to_insert, @intFromPtr(entry.node), address_to_insert);
            self.root = new_root;
            self.height += 1;
            return left_root;
        }
    };
}
