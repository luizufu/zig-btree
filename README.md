# zig-btree

A [B+ tree](https://en.wikipedia.org/wiki/B%2B_tree) implementation using [zig](https://ziglang.org/).

## Operations

Creating btrees:

```zig

fn lessThan(context: void, a: u32, b: u32) Order {
    _ = context;
    return std.math.order(a, b);
}

// a B+ tree with key u32, value u32 and branch factor 8
// it will use no context (void) and the lessThan function to compare keys
var btree = try Btree(u32, u32, 8, void, lessThan).init(allocator, {});
defer btree.deinit();

// a B+ tree with key u32, value u32 and maximum node size 512 bytes
var btree = try BtreeWithMaxNodeSize(u32, u32, 512, void, lessThan).init(allocator, {});
defer btree.deinit();

const numberOfElements = btree.len;
const heightOfTheTree = btree.height;
```

Inserting/Updating:

```zig
// btrees only manage value pointers
const successfullyInserted: bool = try btree.insert(key, &value);

const optionalOldPointer: ?*V = btree.tryUpdate(key, &anotherValue);

// if no old pointer is returned, then a new key was inserted
const optionalOldPointer: ?*V = try btree.insertOrUpdate(key, &anotherValue);
```

Removing:

```zig
const optionalValuePointer: ?*V = try btree.remove(key);

// remove all elements such that leftKey <= element < rightKey
// it returns a btree containing all removed elements
var removedBtree: Btree(...) = try btree.removeRange(leftKey, rightKey); 

// the nodes of the returned btree can be reused if necessary
try btree.recycleTreeNodes(&removedBtree);
```

Finding: 

```zig
const optionalValuePointer: ?*V = btree.find(key);
```

Iterating:

```zig
var it = btree.iterator();
while (it.next()) |entry| {
    const key: K = entry.key;
    const valuePointer: *V = entry.value;
    ...
}

var it = btree.lowerBoundIterator(key);
while (it.next()) |entry| {
    ...
}

var it = btree.upperBoundIterator(key);
while (it.next()) |entry| {
    ...
}
```

Others:

```zig
// access the minimum element
const optionalEntry: .(.key: K, .value: *V) =  btree.min();

// access the maximum element
const optionalEntry: .(.key: K, .value: *V) =  btree.max();

// split btree such that elems(leftBtree) < key and elems(rightBtree) >= key
var rightBtree = try leftBtree.split(key);

// the split operation does not update btree.len, use this if necessary
leftBtree.recomputeLen();
rightBtree.recomputeLen();
...
leftBtree.len // now it is safe to access it 

// two btrees can be combined again
// lefBtree will consume rightBtree entirely, no need to deinit after
// requirement: leftBtree.max < rightBtree.min
try leftBtree.concatenate(rightBtree);
```

## Benchmark

To run the benchmark do:

```sh
cd benchmark
zig build run -Drelease-fast=true > results.csv
chmod +x create-plot.r
./create-plot.r
myimageviewer plot.jpg
```

It requires an [R](https://www.r-project.org/) environment with ggplot2 and reshape2.

Here is the benchmark I ran on my computer.

![Benchmark](benchmark/plot.jpg)

